// alert("hello, b273!")

// In JS, classes can be created using the "class" keyword and {}

/* 

Naming convention for classes: Begins with Uppercase characters

    Syntax:
        class NameOfClass {

        }
*/

/* 
class Student {
    constructor(name, email) { // to add properties for class
        this.name = name;
        this.email = email;
    }
}

// instantiates an object
let studentOne = new Student('John', 'john@mail.com'); 
let studentTwo = new Student();
*/

/*

    Mini-Activity:
        Create a new class called Person

        This person should be able to instantiate a new object with the ff fields

        name,
        age (should be a number and must be greater than or equal to 18, otherwise set the propery to undefined),
        nationality,
        address

        Instantiate 2 new objects from the Person class: person1 and person2

*/

class Person {
    constructor(name, age, nationality, address) {
        this.name = name;
        this.nationality = nationality;
        this.address = address;
        
        if (typeof age !== "number" && age >= 18) {
            this.age = age;
        }
        else {
            this.age = undefined;
        }
    }
}

// let personOne = new Person('Marina', 17, 'American', 'LA'); 
// let personTwo = new Person('Minty', 18, 'Australian', 'NSW');
// console.log(personOne);
// console.log(personTwo);

// Activity 1

/* 
    1. What is the blueprint where objects are created from?

    Class

    2. What is the naming convention applied to classes?

    Begins with Uppercase characters

    3. What keyword do we use to create objects from a class?

    Class

    4. What is the technical term for creating an object from a class?

    Instantiating

    5. What class method dictates HOW object will be created from that class?
    
    Constructor method

*/
/*
class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    
    if (Array.isArray(grades) && grades.length === 4 && grades.every(grade => typeof grade === 'number' && grade >= 0 && grade <= 100)) {
        this.grades = grades;
    } else {
        this.grades = undefined;
    }
  }
}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
let student1 = new Student('John', 'john@mail.com', [84, 78, 88]);
let student2 = new Student('John', 'john@mail.com', [-10, 84, 78, 88]);
let student3 = new Student('John', 'john@mail.com', ['hello', 84, 78, 88]);

console.log(studentOne);
console.log(student1);
console.log(student2);
console.log(student3);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);
*/

// Activity 2

/* 
    1. Should class methods be included in the class construtor?

    No

    2. Can class methods be separated by commas?

    No

    3. Can we update an object's properties via dot notation?

    Yes

    4. What do you call the methods used to regulate access to an object's properties?

    Getter and setter

    5. What does a method need to return in order for it to be chainable?

    this method

*/

class Student {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.gradeAvg = undefined; // returns a number data type
        this.passed = undefined;
        this.passedWithHonors = undefined;

    if (grades.length === 4) {
        if (grades.every(grade => grade >= 0 && grade <= 100)) {
            this.grades = grades;
        } else {
            this.grades = undefined;
        }
    } else {
            this.grades = undefined;
        }
    }

    // Methods
    login() {
        console.log(`${this.email} has logged in.`);
        // console.log(this);
        return this; // to return the object after displaying the message
    }

    logout() {
        console.log(`${this.email} has logged out.`);
        return this; // return statement cancels the code after it
    }

    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}.`);
        return this;
    }

    computeAvg() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        // update property
        this.gradeAvg = sum/4;
        // return object
        return this;
        //return sum/4;
    }

    willPass() {
        this.passed = this.computeAvg().gradeAvg >= 85 ? true : false;
        return this;
        // if(this.gradeAvg >= 85) {
        //     this.passed = true;
        //     return this;
        // } else {
        //     this.passed = false;
        //     return this;
        // }
        // return this.computeAvg() >= 85 ? true : false && this;
    }

    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAvg >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = undefined;
            return this;
        }
        
        // if (this.willPass()) {
        //     if (this.gradeAvg >= 90) {
        //         this.passedWithHonors = true;
        //         return this;
        //     } else {
        //         this.passedWithHonors = false;
        //         return this;
        //     }
        // } else {
        //     this.passedWithHonors = undefined;
        //     return this;
        // }
    }
}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);

// Getter and Setter
    // Best practice dictates that we regulate access to such properties or methods

    // Getter - retrieval
    // Setter - manipulation or updating the value of property


